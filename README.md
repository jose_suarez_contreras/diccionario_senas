**DICCIONARIO TÉCNICO DE INGENIERIA EN LA LENGUA DE SEÑAS VENEZOLANA**

**Autor:** José  Gregorio Suarez Contreras

Lista de palabras:

- 1- Aceleración
- 2- Algoritmo
- 3- Bases
- 4- Básica
- 5- Cálculo
- 6- Calidad
- 7- Circuito
- 8- Dato
- 9- Electricidad
- 10- Física
- 11- Fórmula
- 12- Grafos
- 13- Homogéneas
- 14- Horizontal
- 15- Instrumentación
- 16- Integrales
- 17- Lengua
- 18- Ley-de-ohm
- 19- Maple
- 20- Matemáticas
- 21- Matlab
- 22- Mecánica
- 23- Operación
- 24- Operativo
- 25- Polinomio
- 26- Practicar
- 27- Química
- 28- Redes
- 29- Representación
- 30- Servicio

Ver el diccionario en la web: https://diccionario-senas-jose-suarez-contreras-c36b714538d847088afae10.gitlab.io/
